<?php

namespace Drupal\image_style_usage\Controller;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Displays the usage of image styles across all entities' view modes and views.
 */
class ImageStyleUsageController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs the UmiraniConsultationRoomController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): object {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * Loads entity view displays grouped by entity type and bundle.
   *
   * @return \Drupal\Core\Entity\Display\EntityViewDisplayInterface[][][]
   *   Entity view displays grouped by entity type and bundle.
   */
  protected function loadEntityViewDisplays(): array {
    /** @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $entity_view_display_storage */
    $entity_view_display_storage = $this->entityTypeManager->getStorage('entity_view_display');
    $query = $entity_view_display_storage->getQuery()
      ->accessCheck(TRUE)
      ->sort('id');
    $ids = $query->execute();
    $entities = $entity_view_display_storage->loadMultipleOverrideFree($ids);
    uasort($entities, [$entity_view_display_storage->getEntityClass(), 'sort']);
    $grouped_entities = [];
    foreach ($entities as $entity) {
      $grouped_entities[$entity->getTargetEntityTypeId()][$entity->getTargetBundle()][] = $entity;
    }
    return $grouped_entities;
  }

  /**
   * Collects the definitions of fields whose display is configurable.
   *
   * @param string $entity_type
   *   Entity type to get field definitions for.
   * @param string $entity_bundle
   *   Entity bundle to get field definitions for.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   The array of field definitions
   */
  protected function getFieldDefinitions(string $entity_type, string $entity_bundle): array {
    $context = 'view';
    return array_filter($this->entityFieldManager->getFieldDefinitions($entity_type, $entity_bundle), function (FieldDefinitionInterface $field_definition) use ($context) {
      return $field_definition->isDisplayConfigurable($context);
    });
  }

  /**
   * Returns the extra fields of the entity type and bundle used by this form.
   *
   * @param string $entity_type
   *   Entity type to get extra field definitions for.
   * @param string $entity_bundle
   *   Entity bundle to get extra field definitions for.
   *
   * @return array
   *   An array of extra field info.
   */
  protected function getExtraFields(string $entity_type, string $entity_bundle): array {
    $context = 'display';
    $extra_fields = $this->entityFieldManager->getExtraFields($entity_type, $entity_bundle);
    return $extra_fields[$context] ?? [];
  }

  /**
   * Displays the usage of image styles.
   */
  public function usage() {
    $entity_view_mode_storage = $this->entityTypeManager->getStorage('entity_view_mode');

    // Get all image styles and consider them unused.
    $image_style_storage = $this->entityTypeManager->getStorage('image_style');
    $unused_image_styles = $image_style_storage->loadMultiple();

    // Collect all image style instances in view displays.
    $image_styles_instances = [];
    $grouped_entity_view_displays = $this->loadEntityViewDisplays();
    foreach ($grouped_entity_view_displays as $entity_type => $bundle_entity_view_displays) {
      foreach ($bundle_entity_view_displays as $bundle => $entity_view_displays) {
        $field_definitions = array_merge($this->getFieldDefinitions($entity_type, $bundle), $this->getExtraFields($entity_type, $bundle));
        foreach ($entity_view_displays as $entity_view_display) {
          $content_fields = $entity_view_display->get('content');
          foreach ($content_fields as $field_name => $field_config) {
            if (empty($field_config['type'])) {
              continue;
            }
            if ($field_config['type'] == 'image') {
              if (!empty($field_config['settings']['image_style'])) {
                unset($unused_image_styles[$field_config['settings']['image_style']]);
                $entity_type_entity = $this->entityTypeManager->getDefinition($entity_type);
                try {
                  $bundle_storage = $this->entityTypeManager->getStorage($entity_type . '_type');
                }
                catch (PluginNotFoundException $e) {
                  $bundle_storage = NULL;
                }
                $view_mode_entity = $entity_view_mode_storage->load($entity_type . '.' . $entity_view_display->getMode());
                $image_styles_instances[$field_config['settings']['image_style']][] = [
                  'image_type' => $this->t('Image'),
                  'instance_type' => 'entity_view_display',
                  'instance_type_label' => $this->t('Entity view display'),
                  'entity_type' => $entity_type,
                  'entity_type_label' => $entity_type_entity->getLabel(),
                  'bundle' => $bundle,
                  'bundle_label' => $bundle_storage ? $bundle_storage->load($bundle)
                    ->label() : NULL,
                  'view_mode' => $entity_view_display->getMode(),
                  'view_mode_label' => $view_mode_entity ? $view_mode_entity->label() : 'Default',
                  'field' => $field_name,
                  'field_label' => isset($field_definitions[$field_name]) ? $field_definitions[$field_name]->getLabel() : $field_name,
                ];
              }
            }
            elseif ($field_config['type'] == 'responsive_image') {
              if (!empty($field_config['settings']['responsive_image_style'])) {
                $responsive_image_style_storage = $this->entityTypeManager->getStorage('responsive_image_style');
                $responsive_image_style = $responsive_image_style_storage->load($field_config['settings']['responsive_image_style']);
                if ($responsive_image_style) {
                  $entity_type_entity = $this->entityTypeManager->getDefinition($entity_type);
                  try {
                    $bundle_storage = $this->entityTypeManager->getStorage($entity_type . '_type');
                  }
                  catch (PluginNotFoundException $e) {
                    $bundle_storage = NULL;
                  }
                  $view_mode_entity = $entity_view_mode_storage->load($entity_type . '.' . $entity_view_display->getMode());

                  // Collect image styles based on responsive image config.
                  $image_styles = [];
                  if ($responsive_image_style->getFallbackImageStyle()) {
                    $image_styles[$responsive_image_style->getFallbackImageStyle()] = TRUE;
                  }
                  foreach ($responsive_image_style->getImageStyleMappings() as $image_style_mapping) {
                    if ($image_style_mapping['image_mapping_type'] == 'image_style') {
                      if (!empty($image_style_mapping['image_mapping'])) {
                        $image_styles[$image_style_mapping['image_mapping']] = TRUE;
                      }
                    }
                    elseif ($image_style_mapping['image_mapping_type'] == 'sizes') {
                      if (!empty($image_style_mapping['image_mapping']['sizes_image_styles'])) {
                        foreach ($image_style_mapping['image_mapping']['sizes_image_styles'] as $size_image_style) {
                          $image_styles[$size_image_style] = TRUE;
                        }
                      }
                    }
                  }

                  // Add responsive image style instances.
                  foreach (array_keys($image_styles) as $image_style) {
                    unset($unused_image_styles[$image_style]);
                    $image_styles_instances[$image_style][] = [
                      'image_type' => $this->t('Responsive image'),
                      'instance_type' => 'entity_view_display',
                      'instance_type_label' => $this->t('Entity view display'),
                      'entity_type' => $entity_type,
                      'entity_type_label' => $entity_type_entity->getLabel(),
                      'bundle' => $bundle,
                      'bundle_label' => $bundle_storage ? $bundle_storage->load($bundle)
                        ->label() : NULL,
                      'view_mode' => $entity_view_display->getMode(),
                      'view_mode_label' => $view_mode_entity ? $view_mode_entity->label() : 'Default',
                      'field' => $field_name,
                      'field_label' => isset($field_definitions[$field_name]) ? $field_definitions[$field_name]->getLabel() : $field_name,
                    ];
                  }
                }
              }
            }
          }
        }
      }
    }

    // Collect image style instances in views.
    $view_entity_storage = $this->entityTypeManager->getStorage('view');
    $views = $view_entity_storage->loadMultiple();
    foreach ($views as $view) {
      /** @var \Drupal\views\Entity\View $view */
      $displays = $view->get('display');
      foreach ($displays as $display) {
        if (!empty($display['display_options']['fields'])) {
          foreach ($display['display_options']['fields'] as $field_config) {
            if (empty($field_config['type'])) {
              continue;
            }
            if ($field_config['type'] == 'image') {
              if (!empty($field_config['settings']['image_style'])) {
                unset($unused_image_styles[$field_config['settings']['image_style']]);
                $image_styles_instances[$field_config['settings']['image_style']][] = [
                  'image_type' => $this->t('Image'),
                  'instance_type' => 'views',
                  'instance_type_label' => $this->t('Views'),
                  'view' => $view->id(),
                  'view_label' => $view->label(),
                  'display' => $display['id'],
                  'display_label' => $display['display_title'],
                  'field' => $field_config['entity_field'],
                  'field_label' => $field_config['label'],
                ];
              }
            }
          }
        }
      }
    }

    // Process unused image styles.
    $output = $rows = [];
    foreach ($unused_image_styles as $image_style) {
      $rows[] = [
        Link::fromTextAndUrl($image_style->label(), $image_style->toUrl())
          ->toString(),
      ];
    }
    if ($rows) {
      $output[] = [
        '#prefix' => '<h2>' . $this->t('Unused image styles') . '</h2>',
        '#type' => 'table',
        '#header' => [$this->t('Image style')],
        '#rows' => $rows,
      ];
    }

    // Process used image styles.
    foreach ($image_styles_instances as $image_style => $image_style_instances) {
      $rows = $link_texts = [];
      foreach ($image_style_instances as $image_style_instance) {
        $row = [];
        switch ($image_style_instance['instance_type']) {
          case 'entity_view_display':
            $link_text = $image_style_instance['entity_type_label'];
            if ($image_style_instance['bundle_label']) {
              $link_text .= ' -> ' . $image_style_instance['bundle_label'];
            }
            $link_text .= ' -> ' . $image_style_instance['view_mode_label'] . ' -> ' . $image_style_instance['field_label'];
            if (!isset($link_texts[$link_text])) {
              $link_texts[$link_text] = TRUE;
              $route_parameters = [];
              if ($image_style_instance['view_mode'] == 'default') {
                $route_name = 'entity.entity_view_display.' . $image_style_instance['entity_type'] . '.default';
                $route_parameters[$image_style_instance['entity_type'] . '_type'] = $image_style_instance['bundle'];
              }
              else {
                $route_name = 'entity.entity_view_display.' . $image_style_instance['entity_type'] . '.view_mode';
                $route_parameters[$image_style_instance['entity_type'] . '_type'] = $image_style_instance['bundle'];
                $route_parameters['view_mode_name'] = $image_style_instance['view_mode'];
              }
              $row[0] = $image_style_instance['instance_type_label'];
              $row[1] = $image_style_instance['image_type'];
              $row[2] = Link::fromTextAndUrl($link_text, new Url($route_name, $route_parameters))
                ->toString();
            }
            break;

          case 'views':
            $link_text = $image_style_instance['view_label'] . ' -> ' . $image_style_instance['display_label'] . ' -> ' . $image_style_instance['field_label'];
            $row[0] = $image_style_instance['instance_type_label'];
            $row[1] = $image_style_instance['image_type'];
            $row[2] = Link::fromTextAndUrl($link_text, new Url('entity.view.edit_display_form', [
              'view' => $image_style_instance['view'],
              'display_id' => $image_style_instance['display'],
            ]))->toString();
            break;
        }

        if ($row) {
          $rows[] = $row;
        }
      }

      if ($rows) {
        $image_style_storage = $this->entityTypeManager->getStorage('image_style');
        $image_style_entity = $image_style_storage->load($image_style);
        // Sort rows by field name.
        ksort($rows);
        $output[] = [
          '#prefix' => '<h2>' . ($image_style_entity ? $image_style_entity->label() : $image_style) . '</h2>',
          '#type' => 'table',
          '#header' => [
            $this->t('Instance type'),
            $this->t('Image type'),
            $this->t('Instance'),
          ],
          '#rows' => $rows,
        ];
      }
    }
    return $output;
  }

}

Image style usage

A simple module that tracks the image styles usage in views and entity view displays. It also tracks the responsive image styles usage in the entity view displays. The image style usage is displayed under the path "/admin/config/media/image-styles/usage".

The module does not scan the code, so if there is an instance of the image style in the code or template file, these are not included.
